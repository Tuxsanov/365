
export const laungageList = [
  {
    name: 'Dutch',
    value: 'dutch',
  },
  {
    name: 'French',
    value: 'french',
  },
  {
    name: 'German',
    value: 'german',
  },
];

export const dropDownData = [
  {
    name: 'Item1',
    value: 'item1',
  },
  {
    name: 'Item2',
    value: 'item2',
  },
  {
    name: 'Item3',
    value: 'item3',
  },
];
