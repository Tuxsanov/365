import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    // flex: 1,
    paddingTop: 20,
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
  },
  language: {
    marginHorizontal: 20,
  },
  main: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 40,
  },
  logo: {
    // marginBottom: 20,
    // left: -15,
    width: 135,
    height: 28,
  },
  btn: {
    marginTop: 10,
    marginBottom: 6,
    padding: 6,
  },
});
