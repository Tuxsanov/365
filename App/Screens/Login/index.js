import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Image,
  StatusBar,
} from 'react-native';
import {
  Button,
  TextInput,
  Text,
} from 'react-native-paper';
import { laungageList } from '@services/staticData'
import styles from './style';

const deviceHeight = Dimensions.get('window').height;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInput: '',
      laungageList,
    };
    this._handleButton = this._handleButton.bind(this);
  }

  componentWillMount() {
    const { navigator } = this.props;
  }


  _handleButton() {
    const { navigator } = this.props;
    navigator.resetTo({
      screen: 'User',
      animationType: 'slide-down',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  render() {
    const { textInput, laungageList } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#fff"
          barStyle="dark-content"
        />
        {/* <Text style={styles.language}>Language: Uzbek</Text> */}
        <View style={{ 
          backgroundColor: '#fff',
          width: '100%',
          height: '100%',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <Image
            style={styles.logo}
            source={{ uri: 'https://image.ibb.co/mFfAKU/Layer-0.png' }}
          />
          {/* <Text style={{ marginTop: 6 }}>Loading...</Text> */}
          <View style={{ width: 260, marginTop: 30 }}>
            <TextInput
              label='+7 (***) *** ** **'
              value={this.state.text}
              mode="outlined"
              style={{ width: '100%', backgroundColor: '#fff', }}
            />
            <Button style={styles.btn} mode="contained" 
            mode="contained" onPress={() => this._handleButton()}
            >
              Kirish
            </Button>
            <Text style={{ fontSize: 12 }}>
              * Malumotlar ishonchligi va xafsizligini taminlash maqsadida vaqtinchalik telefon raqam orqali ruyxatdan utishi tasdiqlab quyilgan.
            </Text>
            <Text style={{ fontSize: 12, marginTop: 4 }}>** Foydalanuvchi malumotlari shitrixlangan kod asosida saqlanadi va uning malumotlari hech kimga oshkor etilmaydi.</Text>
          </View>
        </View>
        {/* <View style={styles.main}>
          <View>
            <Image
              style={styles.logo}
              source={{ uri: 'https://image.ibb.co/eMEWzU/logo.png' }}
            />
          </View>

          <TextInput
            label='+7 (***) *** ** **'
            value={this.state.text}
            mode="outlined"
          />
          <Button style={styles.btn} mode="contained" 
          mode="contained" onPress={() => this._handleButton()}
          >
            Kirish
          </Button>
          <Text>
            * Malumotlar ishonchligi va xafsizligini taminlash maqsadida vaqtinchalik telefon raqam orqali ruyxatdan utishi tasdiqlab quyilgan.
          </Text>
          <Text>** Foydalanuvchi malumotlari shitrixlangan kod asosida saqlanadi va uning malumotlari hech kimga oshkor etilmaydi.</Text>
        </View> */}
      </View>
    );
  }
}
