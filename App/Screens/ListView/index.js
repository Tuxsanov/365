import React, { Component } from 'react';
import {
  View,
  FlatList,
  ImageBackground,
} from 'react-native';
import {
  Appbar,
} from 'react-native-paper';
import { DropDownList } from '@components/DropDownList'
import { RenderItem } from '@components/RenderItem';
import { dropDownData } from '@services/staticData';

import styles from './style';

export default class ListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropDownData,
      listData: [1, 2, 3, 4, 5],
      numColumnsTwo: false,
      isRefreshing: false,
    };
    this.timeOut = null;
    this._onRefresh = this._onRefresh.bind(this);
    this._handleListView = this._handleListView.bind(this);
  }

  componentWillMount() {
    const { navigator } = this.props;
  }

  _onRefresh() {
    this.setState({
      isRefreshing: false,
    });
  }

  // handle toggling of View
  _handleListView() {
    this.setState({
      numColumnsTwo: !this.state.numColumnsTwo,
    });
  }

  // to open side Drawer
  _handleDrawer() {
    const { navigator } = this.props;
    navigator.toggleDrawer({
      side: 'left',
      animated: true,
      to: 'open',
    });
  }

  render() {
    const {
      listData,
      numColumnsTwo,
      isRefreshing,
    } = this.state;

    return (
      <View style={styles.main}>
        {/* header */}
        <Appbar.Header>
          <Appbar.Action
            icon="reorder"
            onPress={() => {
              this._handleDrawer();
            }}
          />
          <Appbar.Content
            title="Title"
            subtitle="Subtitle"
          />
          <Appbar.Action icon="search" onPress={() => console.log('Pressed')} />
          <Appbar.Action icon="more-vert" onPress={() => console.log('Pressed')} />
        </Appbar.Header>
        {/* header complete */}
        <View style={styles.container}>
          {/* DropDowns */}
          <View style={styles.dropDownContainer}>
            <View style={styles.dropDown}>
              <DropDownList title="DropDown" listData={dropDownData} />
            </View>
            <View style={styles.dropDown}>
              <DropDownList title="DropDown" listData={dropDownData} />
            </View>
          </View>

          {/* render toggle View */}
          <Appbar style={{marginBottom: 10,}}>
            <Appbar.Action
              style={styles.toggleIcon}
              icon="apps"
              onPress={() => { this._handleListView(); }}
            />
          </Appbar>

          <FlatList
            style={{ marginTop: 30 }}
            onRefresh={() => this._onRefresh()}
            refreshing={isRefreshing}
            showsVerticalScrollIndicator={false}
            columnWrapperStyle={numColumnsTwo && { justifyContent: 'space-around' }}
            key={(numColumnsTwo ? 'show' : 'hide')}
            numColumns={numColumnsTwo ? 2 : 1}
            style={styles.flatList}
            data={listData}
            keyExtractor={index => index.toString()}
            renderItem={item => (<RenderItem item={item} index={item.index} />)}
          />
        </View>
      </View>
    );
  }
}
