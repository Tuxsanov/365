import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
  },
  dropDownContainer: {
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropDown: {
    flex: 1,
    paddingLeft: 5,
  },
  toggleIcon: {
    position: 'absolute',
    right: 5,
  },
  flatList: {
    marginHorizontal: 15,
  },
});
