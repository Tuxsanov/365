import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Image,
  StatusBar,
  ScrollView,
} from 'react-native';
import {
  Button,
  TextInput,
  Text,
  Appbar,
  List,
} from 'react-native-paper';
import { laungageList } from '@services/staticData'
import styles from './style';

const deviceHeight = Dimensions.get('window').height;

export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInput: '',
      laungageList,
      deviceHeight,
    };
    this._handleButton = this._handleButton.bind(this);
  }

  componentWillMount() {
    const { navigator } = this.props;
  }


  _handleButton() {
    const { navigator } = this.props;
    navigator.push({
      screen: 'Login',
      animationType: 'slide-horizontal',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  render() {
      const { textInput, laungageList, deviceHeight } = this.state;
    //   StatusBar.setBackgroundColor('#fff', true);
    //   StatusBar.setBarStyle('dark-content', true);
      return (
          <View>  
            <Appbar.Header>
                <Appbar.Action
                    icon="arrow-back"
                    onPress={() => {
                    this._handleButton();
                    }}
                />
                <Appbar.Content
                    title="Moskva (uzb)"
                    subtitle="+1k Reklama / +5k Foydalanuvhi"
                />
                {/* <Appbar.Action icon="search" onPress={() => console.log('Pressed')} /> */}
                <Appbar.Action icon="more-vert" onPress={() => console.log('Pressed')} />
            </Appbar.Header>
            
            <ScrollView>
                <List.Item
                    title="Huquqiy yordam"
                    description="Barcha turdagi huquqiy yordamlar"
                    left={props => <List.Icon {...props} icon="gavel" />}
                    right={props => <List.Icon {...props} icon="notifications-active" />}
                />
                <List.Item
                    title="Taxi"
                    description="Moskvaga kelish va ketish"
                    left={props => <List.Icon {...props} icon="directions-car" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="Sport"
                    description="Futbol, basketbol, vallebol"
                    left={props => <List.Icon {...props} icon="golf-course" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="Ovaqatlanish"
                    description="Ovqatlanish markazlari"
                    left={props => <List.Icon {...props} icon="local-dining" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="Ish"
                    description="Ish urinlari"
                    left={props => <List.Icon {...props} icon="local-offer" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="Turar joy"
                    description="Kvartira, xona, hostel"
                    left={props => <List.Icon {...props} icon="local-hotel" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="O'qish"
                    description="Universitet, college va maktablar"
                    left={props => <List.Icon {...props} icon="local-library" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="Shifoxonalar"
                    description="Turli xil kassaliklar buyicha"
                    left={props => <List.Icon {...props} icon="local-hospital" />}
                    right={props => <List.Icon {...props} icon="notifications-none" />}
                />
                <List.Item
                    title="Do'konlar"
                    description="Turli xil turdagi do'konlar"
                    left={props => <List.Icon {...props} icon="shopping-basket" />}
                    right={props => <List.Icon {...props} icon="notifications-active" />}
                />
            </ScrollView>
        </View>
    //   <View style={styles.container}>
    //   </View>
    );
  }
}
