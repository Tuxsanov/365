import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import styles from './style';

export default class Drawer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  
  static navigatorStyle = {
    navBarHidden : true,
  }

  _handleclose() {
    const { navigator } = this.props;
    navigator.toggleDrawer({
      side: 'left',
      animated: true,
      to: 'close',
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.close} onPress={() => this._handleclose()}>
          <Text>
            Close
          </Text>
        </TouchableOpacity>
        <Text>
          DRAWER SCREEN
        </Text>
      </View>
    );
  }
}
