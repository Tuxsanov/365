export const _CityListData = [{
  icon: 'gavel',
  title: 'Huquqiy yordam',
  description: 'Barcha turdagi huquqiy yordamlar',
  notifications: true,
}, {
  icon: 'directions-car',
  title: 'Taxi',
  description: 'Taxi xizmatlari',
  notifications: false,
}, {
  icon: 'golf-course',
  title: 'Sport',
  description: 'Sport turnirlari',
  notifications: false,
}, {
  icon: 'local-dining',
  title: 'Ovaqatlanish',
  description: 'Ovqatlanish markazlari',
  notifications: true,
}, {
  icon: 'local-offer',
  title: 'Ish',
  description: 'Ish urinlari',
  notifications: false,
}, {
  icon: 'local-hotel',
  title: 'Turar joy',
  description: 'Kvartira, xona, hostel',
  notifications: false,
}, {
  icon: 'local-library',
  title: 'O\'qish',
  description: 'Universitet, college va maktablar',
  notifications: false,
}, {
  icon: 'local-hospital',
  title: 'Shifoxonalar',
  description: 'Turli xil kassaliklar buyicha',
  notifications: false,
}, {
  icon: 'shopping-basket',
  title: 'Do\'konlar',
  description: 'Turli xil turdagi do\'konlar',
  notifications: false,
}];

export const CityListData = _CityListData