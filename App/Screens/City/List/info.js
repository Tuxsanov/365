import React, { Component } from 'react';

import {
  List,
} from 'react-native-paper';

export default class CityListRender extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      icon,
      text,
      description,
      notifications,
    } = this.props;
    return (
      <List.Item
        title={text}
        description={description}
        left={props => <List.Icon {...props} icon={icon} />}
        right={props => <List.Icon {...props} icon={notifications ? 'notifications-active' : 'notifications-none'} />}
      />
    );
  }
}
