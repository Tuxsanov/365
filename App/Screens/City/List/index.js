import React, { Component } from 'react';
import {
  View,
  FlatList,
} from 'react-native';
import {
  Appbar,
} from 'react-native-paper';
import style from './style';
import CityListRender from './info';
import { CityListData } from './data';

export default class CityList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this._handleButton = this._handleButton.bind(this);
  }

  componentWillMount() {}

  _handleButton() {
    const { navigator } = this.props;
    navigator.push({
      screen: 'Login',
      animationType: 'slide-horizontal',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }

  renderItem({ item }) {
    return (
      <CityListRender
        icon={item.icon}
        text={item.title}
        description={item.description}
        notifications={item.notifications}
      />
    );
  }

  render() {
    return (
      <View style={style.container}>
        <Appbar.Header>
          <Appbar.Action
            icon="arrow-back"
            onPress={() => {
              this._handleButton();
            }}
          />
          <Appbar.Content
            title="Moskva (uzb)"
            subtitle="+1k Reklama / +5k Foydalanuvhi"
          />
          <Appbar.Action icon="more-vert" onPress={() => console.log('Pressed')} />
        </Appbar.Header>
        <FlatList
          data={CityListData}
          renderItem={this.renderItem}
          keyExtractor={index => index.toString()}
        />
      </View>
    );
  }
}
