import * as React from 'react';
import { Dropdown } from 'react-native-material-dropdown';

export const DropDownList = (props) => {
  const {
    title,
    listData
  } = props;

  return (
    <Dropdown
      baseColor="green"
      dropdownPosition={0}
      inputContainerStyle={{ borderBottomColor: 'transparent' }}
      dropdownMargins={{ min: 20, max: 16 }}
      label={title}
      data={listData}
    />
  );
};
export default DropDownList;
