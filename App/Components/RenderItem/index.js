import React from 'react';
import { View } from 'react-native';
import {
  Button,
  Card,
  Title,
  Paragraph,
  Surface
} from 'react-native-paper';
import styles from './style';

export const RenderItem = (props) => {
  const {
    item,
    index
  } = props;

  return (
    <Surface style={[styles.card]}>
      <Card style={{ flex: 1 }}>
        <Card.Content>
          <Title>Text</Title>
          <Paragraph>subText</Paragraph>
        </Card.Content>
      </Card>
      <View style={styles.cardFooter}>
        <Button icon="info">Text</Button>
        <Button icon="info">Text</Button>
      </View>
    </Surface>
  );
};

export default RenderItem;