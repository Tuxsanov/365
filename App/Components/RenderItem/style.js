import { StyleSheet, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('screen').height;

export default StyleSheet.create({
  card: {
    height: deviceHeight / 4,
    justifyContent: 'space-between',
    flex: 1,
    marginBottom: 10,
    borderRadius: 5,
    marginHorizontal: 5,
    elevation: 4,
  },
  action: {
    borderWidth: 1,
    alignSelf: 'baseline',
  },
  cardFooter: {
    flexWrap: 'wrap',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
