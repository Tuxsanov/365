import { Navigation } from 'react-native-navigation';
import { Dimensions, Platform } from 'react-native';
import { DefaultTheme, Provider as PaperProvider  } from 'react-native-paper';
import Login from '@screens/Login/index';
import User from '@screens/User/index';
import CityList from '@screens/City/List/index';
import ListView from '@screens/ListView/index';
import DrawerScreen from '@screens/Drawer/index';
import * as React from 'react';


const deviceWidth = Dimensions.get('window').width;
const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#764fea',
    accent: '#ee63d3',
    text: '#112840',
    placeholder: '#112840',
  },
};

const withProvider = (Component) => {
  return class extends React.Component {
    render() {
      return (
        <PaperProvider theme={theme}>
            <Component {...this.props} />
        </PaperProvider>
        );
    }
  };
};


Navigation.registerComponent('Login', () => withProvider(Login));
Navigation.registerComponent('CityList', () => withProvider(CityList));
Navigation.registerComponent('User', () => withProvider(User));
Navigation.registerComponent('ListView', () => withProvider(ListView));
Navigation.registerComponent('Drawer', () => withProvider(DrawerScreen));


Navigation.startSingleScreenApp({
  screen: {
    screen: 'Login',
    title: 'Welcome',
    style: {
      height: 8,
    },
    navigatorStyle: {
      navBarHidden: true,
    },
    navigatorButtons: {},
  },
  drawer: {
    left: {
      screen: 'Drawer',
    },
    style: {
      leftDrawerWidth: Platform.OS === 'ios' ? 80 : deviceWidth,
    },
    animationType: 'slide-down',
    disableOpenGesture: false,
  },
  passProps: {},
  animationType: 'slide-down',
});
